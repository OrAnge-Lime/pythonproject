# Задание 1.
## Установка:
1) Установите пакет через pip: `pip install py_proj --extra-index-url https://gitlab.com/api/v4/projects/41739909/packages/pypi/simple --no-deps` 

## Выполнение:
В корневой папке выполните команду `python3 -m py_proj/main.py $VAR` (укажите входную переменную вместо `$VAR`), результат будет находиться в текстовом файле `res/res.txt`.