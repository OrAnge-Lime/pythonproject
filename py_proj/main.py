import random
import sys
import os 

def generate(len = 8):
    chars = "abcdefghigklmnopqrstuvwxyz1234567890_@#$<>_-?!*/-+"
    pas = ""

    if len < 32 and len > 7:
        for i in range(len):
            pas += random.choice(chars)
    else:
        pass
    
    return pas

if __name__ == '__main__':
    res = generate(int(sys.argv[1]))
    os.makedirs("res", exist_ok = True)
    with open("res/res.txt", "w+") as f:
        f.write(res)